#!/bin/bash
# hugo new site .
git clone $HUGO_THEME_URL themes/theme
hugo -D
hugo server -D --baseURL=http://tinyhappy.fr --bind=0.0.0.0 --appendPort=false
