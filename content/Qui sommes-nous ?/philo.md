---
title: "Philosophie tiny happy"
date: 2021-03-26T08:47:11+01:00
draft: false
categories: ["Qui sommes-nous ?"]
---
![](/images/koka.jpg)
Qu’est ce que la philosophie “Tiny Happy” ?
Mener une vie simple, saine et en harmonie avec la nature dont nous faisons tous partie
Développer des relations de partage et de collaboration entre humains et avec les autres êtres vivants 
Contribuer à la diversité du vivant par nos projets et nos actions
Comprendre le monde qui nous entoure,  jouir de sa beauté et de ses bienfaits en le respectant
Agir chacun à son échelle et tous ensemble en se faisant plaisir
et ainsi rejoindre la tribu des Quokka, les êtres vivants les plus heureux au monde ! :-))
