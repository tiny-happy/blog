---
title: "Qui je suis ?"
date: 2021-08-01T16:04:36+02:00
categories: ["Qui sommes-nous ?"]
featured_image: ""
description: ""
---
Hello!  
Je suis sûre que vous avez envie d’en savoir plus sur qui se cache derrière Tiny Happy… Peut-être cela pourra vous inspirer, ou vous aider à trouver votre voie! Car c’est aussi ça, la vocation de Tiny Happy, accompagner dans la quête de sens et pour trouver le bonheur.

Vers mes 26 ans, lorsque j’ai commencé à douter sur ma carrière professionnelle et à tout remettre en question, j’ai suivi un programme en ligne qui s'appelle “Devenir entrepreneur du changement”*. Dans cette formation, il y avait un chapitre entier consacré à la connaissance de soi, pour découvrir son potentiel. C’est dans ce chapitre que j’ai dû écrire ma “ligne de vie”, afin de visualiser tout le chemin que j’avais parcouru  depuis ma naissance pour comprendre comment j’en étais arrivée là, et vers où je voulais aller. 

Et bien me voilà, Claire, j’ai 27 ans et je dois avouer que j’ai fait un sacré chemin mental pour en arriver là! J’ai grandi dans un petit cocon familial avec un grand frère et des parents que j’admirais (c’est toujours vrai, si vous me lisez... ). Nous avions un chien (une toutoune pour les intimes) et un chat que j’ai toujours!

Nous sommes tous plutôt intellectuels. Nous avions souvent des discussions et débats à n’en plus finir sur la science, la place de l’homme dans le monde, le sens de la vie… Mon père a toujours été philosophe et je pense qu’il nous l’a transmis! 
Ma mère, elle, est très empathique, sensible et m’a transmis des valeurs humaines fortes telles que la bienveillance, l’écoute et le partage. 
Une Claire ingénieure, (hyper)sensible pour ceux qui connaissent, et philosophe sur les bords s’est donc formée au sein de cette famille.

Ma vie, ce sont aussi des rencontres, des gens qui m’ont tendu la main à des moments où je ne pouvais pas ….. vous l’avez ? 
Je disais donc… des rencontres et des amitiés qui m’ont forgée!
Quand j’ai quitté ce cocon pour faire mes études en classes préparatoires scientifiques, j’ai rencontré des gens tellement authentiques et différents que cela m'a véritablement ouverte! En fait, je sortais d' un lycée assez bourgeois dans lequel je ne me retrouvais pas toujours. Me fondre dans le décor, rentrer dans le tableau (oh là, moi et les expressions ça n’a jamais marché) me demandait beaucoup d’efforts. 
Mes études ont été difficiles, j’ai été plusieurs fois confrontée à mes limites, mais j’ai appris à les repousser en m’entourant de mes proches. Et j'ai véritablement commencé à prendre possession de ma personnalité, je n’avais plus de filtre et ça faisait du bien! 
 
J’ai donc eu une enfance assez idyllique, avec une vision d’un monde de bisounours pour être sincère…Quand soudain! *Non ça ne sera pas dramatique je vous rassure *
J’ai démarré la vie professionnelle en CDI dans une grande entreprise textile, en tant qu'ingénieur produit. J’ai énormément appris pendant 4 ans, j’ai eu la chance de voyager en Asie pour rencontrer des fournisseurs, découvrir la chaîne de production de nos vêtements et comprendre tout le processus de conception de produits. 
J’ai aussi découvert un monde de l’entreprise assez compétitif, où l’on doit rendre compte régulièrement de ce que l’on fait, et finalement assez inégal lorsque je comparais ma situation à celle de nos partenaires fournisseurs que je visitais à l’autre bout du monde. 
Mes voyages m’ont fait découvrir des zones très pauvres, polluées et sales, cela a généré beaucoup de souffrance et de remise en cause du système dans lequel je travaillais. 
Nous étions au cœur des sujets d’éco-conception et de réduction de CO2, mais en parallèle, il fallait sortir des produits toujours moins chers, dont les gens n’ont pas toujours besoin, et vendre toujours plus. Finalement, petit à petit je réalisais que mes valeurs ne s'exprimaient pas dans mon quotidien professionnel… 

Car dans ma vie personnelle, je commençais à changer mes habitudes en cherchant à consommer moins de viande, en allant dans des épiceries vrac, et en cherchant des produits moins polluants et moins mauvais pour la santé. 
Cette distorsion a été difficile à vivre, j’ai souffert d’éco-anxiété, de manque de confiance en moi, bref j’étais vraiment paumée...
Je cherchais à retrouver une certaine harmonie dans ma vie, ne pas vivre deux extrêmes. 

Le confinement m’a offert beaucoup de temps, j’ai donc décidé de me prendre en main! Ne plus laisser tourner le moulin à paroles négatives, mais plutôt de passer à l’action. J’ai donc entrepris cette formation en ligne, je me suis mise au yoga et à la connaissance de soi. J’ai fait du coaching à distance qui m’a aussi aidé à lever les “croyances limitantes”. J’ai été puiser dans mes racines pour écrire mon “blason de vie” 


Tout ce travail d’introspection m’a fait prendre conscience de ce que j’aimais vraiment: accompagner les gens, partager mes expériences et mes aventures, écouter et débattre, prendre soin de la planète (l’homme, les animaux, la nature), refaire le monde en collectif et lever les barrières, trouver des solutions. J’ai toujours été quelqu’un de très optimiste! 

J’ai beaucoup échangé sur mes états d’âmes avec mon entourage proche, j’ai été soutenue et écoutée, et j’ai réussi à embarquer ma famille vers cet idéal de vie que j’imagine… avec Tiny Happy! ;) 

Et vous, quelle est votre histoire et qu’avez-vous envie d’entreprendre pour demain ? 

*https://fr.coursera.org/learn/entrepreneur-changement 







