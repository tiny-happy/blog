---
title: "Le colibri, qu’est ce qu’il nous dit ?"
date: 2021-08-01T16:06:55+02:00
categories: ["Ma planète"]
featured_image: "/images/pexels-colibri-3882092.jpeg"
description: ""
omit_header_text: true
---
J’aimerais commencer par cette fameuse légende du Colibri, je pense que c’est le moment de s’arrêter un peu dessus. Il s’agit d’une légende amérindienne qui a été reprise et racontée par Pierre Rabhi (nous reviendrons sur ce personnage juste après), elle est une véritable source d’inspiration.

*Un jour, dit la légende, il y eut un immense incendie de forêt. Tous les animaux terrifiés, atterrés, observaient impuissants le désastre. Seul le petit colibri s’activait, allant chercher quelques gouttes avec son bec pour les jeter sur le feu. Après un moment, le tatou, agacé par cette agitation dérisoire, lui dit : "Colibri ! Tu n’es pas fou ? Ce n’est pas avec ces gouttes d’eau que tu vas éteindre le feu ! "
Et le colibri lui répondit : "Je le sais, mais je fais ma part."*

## Les messages de cette légende

Le Colibri essaye de nous dire que plutôt que de ne rien faire face à un danger, tout le monde peut agir à son échelle, même le plus petit des gestes aura du mérite. 
Si nous nous posions cette question au fond “quel rôle j’ai envie de jouer, quelle image j’ai envie de montrer à mes proches, à mes enfants, à moi-même ?" Est ce le rôle de l’autruche qui plonge sa tête dans un trou pour ne pas voir le problème, le rôle de celui qui prend la fuite et laisse les autres se débrouiller, ou bien le rôle de celui qui fera tout ce qui est en son pouvoir pour stopper l’incendie ? 

Voilà donc un deuxième message, c’est le courage du colibri. Il ne fait pas simplement sa part, il a le courage d’affronter l’incendie et de décider d’agir. 

Aussi, il montre l’exemple aux autres animaux de la forêt. Car c’est normal d’avoir peur, c’est une réaction naturelle de notre corps. La peur n’est pas un mauvais sentiment, elle est là pour nous faire passer un message : “ralentit, observe, et pose une action ici et maintenant”. Le colibri reste calme, et invite le collectif à agir (un vrai leader malgré sa petite taille!) car à plusieurs ils auront bien plus de chance de pouvoir arrêter cet incendie. 

Le dernier message que je vois, c’est que le colibri utilise la force qui lui est propre : c’est un super pollinisateur, doté d’une extrême vitalité.  Il va donc déposer des gouttes, mais aura la force et la vitesse d’en déposer plusieurs et vite. Cela vient souligner que chacun, selon ses forces et ses qualités, peut agir à sa manière. Je pense que nous avons tous un énorme potentiel au fond de nous et qu’il peut nous donner les ailes et le courage du colibri. 

Cette histoire mérite vraiment d’être diffusée, ainsi qu’aux enfants. Elle véhicule une vision de la vie optimiste, des valeurs de respect et de courage, d’entraide. En voici une version fable pour les enfants : [fable du colibri](https://www.youtube.com/watch?v=lmd4AeYIASA)

## Pierre Rabhi, ce philosophe plein de bon sens

Comme je le disais, c’est Pierre Rabhi qui a repris cette légende pour appeler le collectif à agir pour les problématiques humaines et environnementales auxquelles nous faisons face aujourd’hui. 

Pierre Rabhi est un  agriculteur, philosophe et auteur français né en Algérie. Il est le pionnier de l’agriculture écologique en France. Il est à l’origine du [mouvement Colibris](https://www.colibris-lemouvement.org/), une association qui accompagne les citoyennes et citoyens qui agissent ensemble pour créer un mode de vie plus écologique et solidaire dans leurs quartiers, leurs villes, leurs régions.* C’est une homme plein de sagesse, qui appelle à une “sobriété heureuse” (lien du livre) 


![Pierre Rabhi](/images/Pierre-Rabhi1.jpg)

> *« La sobriété est une option heureuse qui produit une vie allégée, tranquille et libre. Le bonheur n’est pas dans la possession, dans l’avoir, mais dans l’être. »*


## Il faut changer de point de vue

Le parallèle entre cette légende et ce que l'humanité est en train de vivre est assez flagrant. Pour agir comme le colibri et limiter l'embrasement de notre planète, Pierre rabhi ne nous dit pas simplement de moins consommer, mais il parle bien joie de vivre en se concentrant sur l'essentiel, dans l’Être.

>  *"La joie de vivre ne s'achète ni au supermarché ni même dans les magasins de luxe."*

Il n'est ni question de régression, ni de perte de confort. Beaucoup de personnes se sentent écrasés par les enjeux environnementaux, et pensent que ce ne sera pas eux qui feront changer les choses. 
Notre conviction est que l’homme s’est éloigné de la nature dont il fait lui-même partie et qu’il doit retrouver un mode de vie plus harmonieux, sans perdre sa modernité. Le fait d’agir est générateur de bonheur, car il s’agit d’alignement entre nos valeurs et nos actes. 
Savez-vous qu’il suffit d’un échantillon de 15% de la population pour convaincre les 85% restants à suivre dans le changement ? C’est un des principes de la conduite du changement (besoin de ressources)**. 

> *"Soyez le changement que vous voulez voir dans le monde".* Gandhi.

Nous avons tous des vies et des contraintes différentes, mais le point commun entre nous tous c’est le besoin de sécurité, le besoin d’un foyer et le besoin des autres. Prenons soin de nous et de notre foyer pour garder cette sécurité ! Et faisons le ensemble dans le plaisir, en améliorant notre qualité de vie.

## Comment on fait maintenant ?

Ce que nous vous proposons, c'est d'agir à l’image des quokkas plus que du colibri. Le quokka est l’animal surnommé le plus heureux du monde par les australiens, car il est toujours très souriant et véhicule la positivité ! C'est un pacifiste : il fait partie des rares animaux à ne pas entrer en conflit avec ses congénères pour se nourrir ou se reproduire. Aujourd'hui malheureusement le quokka est menacé par la dégradation de son habitat naturel et l'introduction de prédateurs comme les chats et les renards.

![Quokka](/images/quokka.jpg)

Nous pouvons tous nous mettre à l’action en prenant du plaisir, et l'endroit le plus évident pour commencer c'est notre foyer. C'est ce que nous vous proposons chez Tiny Happy, vous accompagner à mettre en place des changements concrets pour mieux vivre chez vous, en harmonie avec la nature. 





