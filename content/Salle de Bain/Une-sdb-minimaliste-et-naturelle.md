---
title: "Une salle de bain minimaliste et naturelle"
date: 2021-08-01T16:44:21+02:00
categories: ["Salle de Bain"]
featured_image: "/images/pexels-sdb-minimaliste-4210314.jpeg"
description: ""
---
## Pourquoi une salle de bain minimaliste ?

Le minimalisme (lien autre article) est un véritable mode de vie, invitant à consommer moins mais mieux. L’objectif étant de se débarrasser du superflu et de se concentrer sur l’essentiel, pour vivre mieux et plus heureux. 
Ce mode de vie peut s’incarner partout dans la maison, mais la salle de bain s’y prête particulièrement car on y entasse bien des produits !
Voici donc nos astuces pour une salle de bain minimaliste.

## Le tri de ses produits, et le rangement.

Nous l’expliquons dans l’article “bien démarrer dans ma salle de bain”, la première étape est de faire un bon nettoyage de printemps en triant les produits accumulés depuis des années et donc périmés, ceux qui sont potentiellement nocifs pour notre santé et celle de l'environnement. Chaque produit a une indication de péremption. Très souvent il est noté 6M ou 12M avec un dessin de pot ouvert (cela signifie que le produit se conserve 6 mois ou 12 mois après ouverture). 
L’objectif n’étant bien sûr pas de jeter tous les produits identifiés dans notre tri, mais peut être de jeter les périmés et de faire ressortir les produits encore “bons” pour les terminer une bonne fois pour toute ! Pourquoi ne pas les regrouper dans un petit panier en osier et les disposer sur une étagère pour nous inciter à les terminer plutôt que de les laisser au fond du placard…
En effet, utiliser des paniers en osier ou autre fibre naturelle permet de créer plusieurs rangements pour organiser les produits de sa salle de bain, et cela apporte un design simple, naturel et élégant. 

## L’essentiel et le naturel font bon ménage

L’idéal est de penser à nos besoins essentiels pour chaque routine : routine du corps, routine du visage, routine des cheveux... Certains produits peuvent très bien être multi-usages, comme les baumes hydratants qui s’utilisent pour hydrater le corps, les mains, les pieds, les lèvres etc. L’objectif étant bien de réduire le nombre de produits pour épurer ses étagères, et donc les rendre plus agréables à regarder. On trouve alors ses produits plus facilement. Et en désencombrant nos espaces on désencombre aussi notre esprit ! 

![Essentiel salle de bain](/images/pexels-essentielsdb-4210371.jpeg)

On privilégie les produits naturels et zéro déchet, qui sont respectueux de l’environnement et de notre santé. Il est très agréable de visualiser des produits purs, naturels et beaux comme ce bloc de savon de Marseille artisanal avec sa petite coupelle et brosse en bois de hêtre. 
Cela inspire confiance et nous invite à nous détendre, à se faire plaisir.

## On invite la nature dans sa salle de bain

L’idéal est de privilégier des matériaux naturels comme le bois, la céramique, le 100% coton, le lin, l’osier pour les meubles et les objets. Le linge de bain est idéalement en coton certifié GOTS (coton biologique), les brosses et brosses à dents en bois, on peut utiliser un verre en céramique par exemple pour les ranger. On choisit les couleurs de la nature : le vert, le terracotta, le bleu océan, le beige pour apporter douceur et sérénité… 
 
On invite également quelques plantes pour nous rapprocher de la nature et aider à purifier l’air de notre salle de bain. L’air étant particulièrement humide, pensez régulièrement à aérer et à disposer des plantes qui recherchent un environnement humide. 
Voici quelques plantes qui peuvent parfaitement convenir et apporter sérénité à votre intérieur : 

- Le Pilea Peperomioides (oreilles de Shrek dans ma langue), très ronde et élégante, à placer dans un coin près d’une lumière naturelle. Elle ne demande pas beaucoup d’eau, la terre doit rester toujours un peu humide. 
![Pilea Peperomioides](/images/plante1sdb.jpeg)

- La Sansevieria qui demande très peu d’entretien, elle trouvera bien sa place dans un coin de votre salle de bain car elle est toute en hauteur, vous n’aurez presque pas à vous en soucier !
![Sansevieria](/images/plante2sdb.jpeg)

- La fameuse Monstera Deliciosa, cette plante tropicale a besoin de beaucoup de lumière donc placez là près d’une fenêtre. 
![Monstera Deliciosa](/images/plante3sdb.jpeg)

## Et dans les WC comment on s’y prend ?

Les toilettes méritent aussi toute notre attention, elles sont parfois dans les salles de bain ou parfois à part.
Pour les mauvaises odeurs, on arrête d’utiliser les sprays industriels qui contiennent des composés organiques volatils et notamment du formaldéhyde. Ces produits sont toxiques et produisent un déchet à la fin de leur utilisation. La meilleure option si vous le pouvez reste d’ouvrir la fenêtre des toilettes pour assainir l’air. Pour les toilettes sans fenêtre, on peut utiliser des bougies naturelles ou un diffuseur d’huiles essentielles, ou encore un spray fait maison. 
Pour nettoyer ses WC, on bannit également les produits industriels toxiques pour l’environnement et notre santé, on utilise du bicarbonate de soude et du vinaigre blanc avec quelques gouttes d’huile essentielle pour assainir avec cette recette de spray wc. Ce spray peut suffire à diffuser une bonne odeur et nettoyer régulièrement. 

Le dernier élément à supprimer pour une salle de bain minimaliste reste la petite poubelle de la salle de bain. Si vous avez réussi à passer au zéro déchet, cette poubelle n’est plus nécessaire (plus de coton tige et coton démaquillant jetable, plus de serviette hygiénique pour les femme grâce aux culottes menstruelles…). Si vous n’êtes pas encore arrivés à cela, l’idéal est de jeter ses déchets dans la poubelle principale de la maison. Car il est finalement assez aberrant d’utiliser des petits sacs poubelles en plastique pour 3 ou 4 déchets par ci par là !

Une salle de bain minimaliste et en harmonie avec la nature, il n'y a rien de plus apaisant... On y retrouve des produits sains et naturels, et des éléments nobles pour se détendre. Alors, qu’attendez-vous pour ramener la nature dans votre intérieur ?
 



