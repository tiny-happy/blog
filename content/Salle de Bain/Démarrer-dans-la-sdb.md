---
title: "Bien démarrer dans ma salle de bain"
date: 2021-08-01T15:36:33+02:00
featured_image: "/images/pexels-Sdb-6781116.jpeg"
description: ""
categories: ["Salle de Bain"]
---

La première pièce pour mieux prendre soin de moi et de la planète m'a semblé assez évidente: la salle de bain.

Prendre soin de soi c'est s'accorder un moment, se chouchouter, se détendre, prendre soin de son corps, de sa peau... Pour cela aujourd'hui, le marketing nous propose un paquet de produits très séduisants pour chaque petit usage. Il faut savoir qu’une femme utilise [en moyenne 16 produits cosmétique chaque jour](https://www.quechoisir.org/actualite-produits-cosmetiques-une-utilisation-reelle-tres-superieure-a-celle-estimee-n49596/) !

La salle de bain n'est pas toujours bien grande, mais il faut avouer qu'on y entasse bien des choses... Non seulement on a beaucoup de produits, mais ces produits contiennent eux aussi beaucoup d'ingrédients que l’on ne sait jamais vraiment déchiffrer. La question est donc  la suivante… savez-vous réellement ce que contiennent vos produits de beauté ? 

## Ce qui se cache dans nos cosmétiques

![cosmétique](/images/pexels-photo-1029896.jpeg)

Les marques industrielles utilisent des ingrédients “marketing” qui sont surtout intéressants pour leur côté glamour et pour la communication, mais qui ne jouent aucun rôle dans l'efficacité du produit. On entend déjà les publicités "hummmmm des bonnes odeurs qui restent sur la peau”, “une mousse siiii onctueuse”, “ce gel douche glisse sur la peau”... 
L’envers du décor c’est qu’elles utilisent des ingrédients qui sont nocifs: des parfums qui contiennent des substances allergènes, des détergents qui irritent la peau, et des ingrédients d'origine pétrolière. 
Le vrai problème vous commencez à le comprendre, c'est que beaucoup de ces ingrédients sont néfastes pour notre santé et la santé de notre planète, allergisants, ou ils perturbent notre fonctionnement hormonal, bref pas terrible quoi!

Vous me direz “oui mais parfois ces ingrédients ne sont présents qu’à 0,02% dans mon produit, ça ne compte pas du coup ?”. Ce qui est dangereux, c’est ce que l’on appelle l’effet cumulatif ou encore l’effet cocktail, car nous utilisons ces produits quotidiennement et comme ils sont présents à petites doses dans plusieurs produits, cela commence à faire beaucoup. 

“Il faut savoir que les scientifiques alertent particulièrement sur les perturbateurs endocriniens, les composants à la toxicité avérée ou suspectée, les conservateurs irritants et allergisants, ou encore les nanomatériaux en raison de leur petite taille”  Clean Beauty.


## Déjà, qu’est ce qu’est un perturbateur endocrinien ? 

Voici la définition de l’OMS : “Un perturbateur endocrinien (PE) désigne une substance ou un mélange qui altère les fonctions du système hormonal et de ce fait induit des effets néfastes dans un organisme intact, chez sa progéniture ou au sein de (sous)-populations”.
Ils pourraient donc favoriser des cancers (notamment  hormonodépendants comme les seins ou les testicules), fragiliser la fertilité ou encore causer d’autres dangers potentiels pour notre santé. 

Voici une liste non exhaustive des ingrédients dont il faut se méfier:

> **Les sulfates tensioactifs**

Ils sont utilisés pour leur effet moussant, ils sont irritants, dessèchent la peau et sont possiblement allergisants.

> **Les  conservateurs**

Beaucoup de conservateurs sont considérés comme des perturbateurs endocriniens (les parabènes, la MIT, le Triclosan…)
Petite remarque: Ne vous fiez pas uniquement à la mention “sans parabènes", car les parabènes ont souvent été remplacés par d'autres conservateurs comme la MIT (methylisothiazolinone) qui lui est un allergène.

> **Les huiles minérales**

Ce sont des huiles issues de la pétrochimie, mieux vaut éviter tout ce qui est pétrolier… (paraffin, ceresin, ozokerite etc).  

> **[Les silicones](https://www.slow-cosmetique.com/le-mag/pourquoi-eviter-les-silicones-dans-les-cosmetiques/)** 

Ils sont présents dans nos shampoings, crèmes, maquillage, etc. Ils sont un vrai danger pour l’environnement car ils mettent des centaines d'années à se dégrader, voire même seulement partiellement lorsqu’ils se retrouvent dans l’eau (ils se retrouvent dans l’eau potable et dans nos déchets avec des cotons démaquillants jetables par exemple). 
 
> **Les sels d’aluminium**

Utilisés dans les déodorants anti-transpirants, ils sont soupçonnés de favoriser le cancer du sein et des maladies neurodégénératives. 
Malheureusement, beaucoup d’industriels continuent d’utiliser ces substances pour éviter d’avoir à reformuler leurs produits ou à modifier leurs emballages, ces reformulations étant évidemment assez compliquées pour eux.**

## Le nettoyage de printemps

Bon ok c'est flippant, mais il est toujours important de comprendre le pourquoi avant d’agir.
Et chez Tiny Happy on cherche surtout à se faire plaisir et à passer à l'action!

La première étape je l’ai donc baptisée “le nettoyage de printemps” : repérer tous les produits dans notre salle de bain qui contiennent ces ingrédients pour les bannir définitivement.
Pour cela, rassurez-vous, c'est facile. Plusieurs applications font le boulot à votre place (lien article), elles scannent vos produits et analysent les ingrédients un à un. Il y a Yuka (la plus connue), qui donne un score global à votre produit, mais personnellement j’aime beaucoup Clean Beauty qui n’attribue pas de note mais vous donne des informations pédagogiques sur les ingrédients controversés et allergènes identifiés. Cela vous permet d’être autonome dans votre étape de grand ménage et de comprendre pourquoi vous choisissez de garder ou de stopper ce produit.

Concernant [les allergènes](https://www.cosmebio.org/fr/nos-dossiers/2018-09-allergenes-parfum-cosmetique/), ils ne sont pas dangereux en soi mais présentent des risques d’irritations ou d’allergies uniquement pour les personnes sensibles à ces substances : rougeurs, démangeaisons… Le but est de les détecter pour que les personnes allergiques puissent les éviter.

Vous verrez qu’au début du tri, on retire 1 produit, puis 2, puis 3... Et pour être très sincère presque toute ma salle de bain y est passée! Mais c'est ce qu'il faut pour repartir sur de bonnes bases.


## Prioriser ses besoins, au naturel

![cosmétique naturel](/images/pexels-photo-6621339.jpeg)

Une fois ce ménage terminé, on arrive sur la deuxième étape “priorisation des besoins”. Ce que j’entends par là, c’est réfléchir quels sont vos besoins et plaisir “indispensables” à votre corps et votre bien-être, et trouver les produits naturels et sains qui répondent à ses besoins. 
Voici quelques exemples: 
- Me laver : avec un savon local et naturel
- M’hydrater la peau car j’ai un peau sèche : avec un baume multi-usage zéro déchet
- Me faire des gommages de temps en temps pour une peau douce : avec un pain exfoliant 
- Hydrater mes pointes car j’ai les cheveux secs : avec de l’huile végétale naturelle
- etc.

Cela permet d’éviter les produits superflus, et de se concentrer sur ce qui compte vraiment. Car après le nettoyage de printemps, on prend aussi conscience du nombre de produits contenus dans des tubes en plastique, qui finissent un jour à la poubelle. 
Une salle de bain minimaliste et naturelle (lien autre article) est tout ce qu’il y a de plus apaisant et de respectueux de la nature. 

Finalement prendre soin de soi dans la salle de bain, c’est respecter son corps et respecter la nature, car sans elle nous n’aurions pas tous ces soins qui nous font du bien...
