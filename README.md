# Blog
Hugo blog deployed via Docker

# Run
## On RPI
```
docker-compose up --build hugo-blog
```
## On AMD64
```
docker-compose -f docker-compose.amd64.yml up
```

# TODO

## NICO
- [ ] Modify gitlab ci workflow (only restart container ?)
- [x] Gérer les sources thème
- [x] Modifier affichage des articles plein écran sur ordinateur
- [x] Regarder template Hugo pour retirer le s systematique pour les taxonomies
- [ ] Changer la police du thème au global pour la Raleway
- [ ] Ajouter un header avec un lien "Notre boutique" (+favicon si possible) qui renvoit vers le site prestashop
- [ ] Créer des catégories pour ranger les articles de blog : Maison, Cosmétiques, Nature et Bien-être, Notre entreprise
- [ ] Footer : lien instagram et facebook OK, supprimer les autres mais ajouter un lien icône enveloppe pour nous contacter (envoyer un mail)
- [ ] Mettre la gestion du blog accessible depuis tout poste de travail (pas que le mac de Claire), donc mettre en commun les ressources

## CLAIRE
- [ ] Faire essais thèmes Lucie Colin

# Mémos

## Github

### Récuperer les sources
- Ouvrir Github
- *pull* pour récupérer les modifications

### Déposer ses modifications
- *commit* les sources modifiées
- *push* pour déposer sur gitlab

## Hugo

### Lancer le serveur
- Ouvrir terminal
- Lancer la commande pour lancer le serveur
```
hugo server
```
- Copier l'adresse du site dans le texte renvoyé par la commande et la coller dans le navigateur internet
```
http://localhost:1313/
```
### Nouvel article
- Pour créer un article lancer la commande
```
hugo new articles/le-nom-de-mon-article.md
```
#### Ajouter une image
Enregistrer l'image dans static/images
puis intégrer l'image dans l'article via la syntaxe 
```
![description image](lien de l'image.jpeg)
```
Plus d'info sur la [syntaxe markdown ici](https://www.markdownguide.org/basic-syntax/).

### Inspecter le blog 
F12 pour inspecter puis icône smartphone pour visualiser blog en mode smartphone
