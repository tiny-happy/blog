FROM debian:buster

# Download and install hugo
ENV HUGO_VERSION 0.83.1
ENV HUGO_BINARY hugo_${HUGO_VERSION}_Linux-ARM64.deb
ENV HUGO_THEME_URL https://github.com/theNewDynamic/gohugo-theme-ananke.git

WORKDIR /usr/share/blog
COPY .  /usr/share/blog

ADD https://github.com/spf13/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BINARY} /tmp/hugo.deb

RUN    apt-get -qq update \
    && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y --no-install-recommends git ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && dpkg -i /tmp/hugo.deb \
    && rm /tmp/hugo.deb \
    && chmod +x start.sh

ENTRYPOINT ["./start.sh"]
